<?php
	/*
	Plugin Name: WooCommerce Free-Kassa Payment Gateway
	Plugin URI: http://www.free-kassa.ru
	Description: Free-Kassa Payment gateway for woocommerce
	Version: 1.0.0
	Author: Cuciurcă Constantin
	Author URI: http://giveforme.16mb.com/
	*/
	



	 


	add_action('plugins_loaded', 'woocommerce_gateway_freekassa_init', 0);


	function woocommerce_gateway_freekassa_init(){
	 
	  	if(!class_exists('WC_Payment_Gateway')) return;
	  	
	 
	  	class WC_Gateway_FreeKassa extends WC_Payment_Gateway{



	    	public function __construct(){

		                    
		        
	    		$this->id = 'freekassa';
	    		$this->icon = apply_filters('woocommerce_freekassa_icon', 'http://blvcks.com/wp-content/uploads/2017/01/fk.png');
		      	$this->medthod_title = 'Free-Kassa';
		      	$this->has_fields = false;
		 
		      	$this->init_form_fields();
		      	$this->init_settings();
		 
		      	$this->title = $this->settings['title'];
		      	$this->merchant_id = $this->settings['merchant_id'];
		      	$this->secret_1 = $this->settings['secret_1'];
		      	$this->secret_2 = $this->settings['secret_2'];
		      	$this->callback_url = $this->settings['callback_url'];
		      	$this->liveurl = 'http://www.free-kassa.ru/merchant/cash.php';
		      	$this->lang = $this->settings['lang'];

		 
		      	$this->msg['message'] = "";
		      	$this->msg['class'] = "";
		      	
			    
                add_action('woocommerce_api_freekassa_callback_response', array(&$this, 'freekassa_callback_response'));

		      	if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
		            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
		        } else {
		            add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
		        }
		        
		        add_action('woocommerce_receipt_freekassa', array(&$this, 'receipt_page'));
	    	}

	    	function init_form_fields(){
 
		       $this -> form_fields = array(
		                'enabled' => array(
		                    'title' => __('Enable/Disable', 'freekassa'),
		                    'type' => 'checkbox',
		                    'label' => __('Enable Free-Kassa Payment Module.', 'freekassa'),
		                    'default' => 'no'),
		                'title' => array(
		                    'title' => __('Title:', 'freekassa'),
		                    'type'=> 'text',
		                    'description' => __('This controls the title which the user sees during checkout.', 'freekassa'),
		                    'default' => __('Free-Kassa', 'freekassa')),
		                'description' => array(
		                    'title' => __('Description:', 'freekassa'),
		                    'type' => 'textarea',
		                    'description' => __('This controls the description which the user sees during checkout.', 'freekassa'),
		                    'default' => __('Pay securely by Credit or Debit card or internet banking through Free-Kassa Secure Servers.', 'freekassa')),
		                'merchant_id' => array(
		                    'title' => __('Merchant ID', 'freekassa'),
		                    'type' => 'text',
		                    'description' => __('This id(Merchant ID) available at "Generate Working Key" of "Settings and Options at Free-Kassa."')),
		                'callback_url' => array(
		                    'title' => __('callback_url', 'freekassa'),
		                    'type' => 'text',
		                    'description' =>  __('http://sitename/?wc-api=freekassa_callback_response', 'freekassa'),
		                ),
		                'secret_1' => array(
		                    'title' => __('Secret word 1', 'freekassa'),
		                    'type' => 'text',
		                    'description' =>  __('Given to Merchant by Free-Kassa', 'freekassa'),
		                ),
		                'secret_2' => array(
		                    'title' => __('Secret word 2', 'freekassa'),
		                    'type' => 'text',
		                    'description' =>  __('Given to Merchant by Free-Kassa', 'freekassa'),
		                ),
		                'lang' => array(
		                    'title' => __('language'),
		                    'type' => 'select',
		                    'options' => array('ru'=>'ru','en'=>'en'),
		                    'description' => "freekassa interface language"
		                )
		        );
		    }	
		 
		    public function admin_options(){

		    	echo '<h3>'.__('Free-Kassa Payment Gateway', 'freekassa').'</h3>';
		       	echo '<p>'.__('Free-Kassa is an payment gateway for online shopping').'</p>';
		       	echo '<table class="form-table">';
		    
		       	// Generate the HTML For the settings form.
		       	$this -> generate_settings_html();
		       	echo '</table>';		 
		    }

		    /**
		     *  There are no payment fields for freekassa, but we want to show the description if set.
		     **/
		    function payment_fields(){
		        if($this->description) echo wpautop(wptexturize($this->description));
		    }
		    
		    /**
		     * Receipt Page
		     **/
		    function receipt_page($order){
		        echo '<p>'.__('Thank you for your order, please click the button below to pay with Free-Kassa.', 'freekassa').'</p>';
		        echo $this -> generate_freekassa_form($order);
		    }
		    
		    /**
		    * Generate freekassa button link
		    **/
		    public function generate_freekassa_form($order_id){
		 
		        global $woocommerce;
		 
		        $order = new WC_Order($order_id);
		        $language = $this->lang;

				$woocommerce->cart->empty_cart();		        

				$out_amount= number_format($order->order_total,2);
				$my_signature = md5($this->merchant_id.":".$out_amount.":".$this->secret_1.":".$order_id);
		        
		        return '<form action="'.$this->liveurl.'" method="get" name="freekassa_form" id="submit_freekassa_payment_form">
		    		<input type="submit" class="button-alt" id="submit_freekassa_payment_form" value="'.__('Pay via Free-Kassa', 'freekassa').'" /> <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancel order &amp; restore cart', 'freekassa').'</a>
		    		<input type="hidden" name="m" value="' . htmlspecialchars($this->merchant_id) . '" />
		    		<input type="hidden" name="oa" value="' . htmlspecialchars($out_amount) . '" />
		    		<input type="hidden" name="s" value="' . htmlspecialchars($my_signature) . '" />
		    		<input type="hidden" name="o" value="' . htmlspecialchars($order_id) . '" />
					<script type="text/javascript">
					<script type="text/javascript">
					jQuery(function(){
					jQuery("body").block(
				        {
				            message: "<img src=\"'.$woocommerce->plugin_url().'/assets/images/ajax-loader.gif\" alt=\"Redirecting…\" style=\"float:left; margin-right: 10px;\" />'.__('Thank you for your order. We are now redirecting you to Payment Gateway to make payment.', 'freekassa').'",
				                overlayCSS:
				        {
				            background: "#fff",
				                opacity: 0.6
				    },
				    css: {
				        padding:        20,
				            textAlign:      "center",
				            color:          "#555",
				            border:         "3px solid #aaa",
				            backgroundColor:"#fff",
				            cursor:         "wait",
				            lineHeight:"32px"
				    }
				    });
				    jQuery("#submit_freekassa_payment_form").click();});</script>
				    </form>
				    ';
		    }

		    /**
		     * Process the payment and return the result
		     **/
		    function process_payment($order_id){
		        $order = new WC_Order($order_id);
		        return array('result' => 'success', 'redirect' => add_query_arg('order',
		            $order->id, add_query_arg('key', $order->order_key, get_permalink(get_option('woocommerce_pay_page_id'))))
		        );
		    }

			
		    function getIP()
		    {
				if(isset($_SERVER['HTTP_X_REAL_IP']))
					return $_SERVER['HTTP_X_REAL_IP'];
				return $_SERVER['REMOTE_ADDR'];
			}

		    function freekassa_callback_response(){


		        global $woocommerce;

		        $merchant = $this->merchant_id;
		        $signature = $this->signature;

		        if (!in_array(getIP(), array('136.243.38.147', '136.243.38.149', '136.243.38.150', '136.243.38.151', '136.243.38.189', '88.198.88.98')))
		        {
					die("hacking attempt!");
				}

				try
				{
					$order_id=$_REQUEST['MERCHANT_ORDER_ID'];
					$order = new WC_Order($order_id);

					if ($_REQUEST['freekassa']=='result') 
					{

						if($order -> status !=='completed')
						{
							$signature=$_REQUEST['SIGN'];
							$out_amount=$_REQUEST['AMOUNT'];

							$my_signature = md5($this->merchant_id.":".$out_amount.":".$this->secret_2.":".$order_id);

							if ($my_signature == $signature)
							{
									$order -> payment_complete();
								    $order -> add_order_note('Free-Kassa payment successful<br/>Unnique Id from Free-Kassa: '.$_REQUEST['intid']);
								    $order -> add_order_note($this->msg['message']);
								    $woocommerce -> cart -> empty_cart();
									die("YES");
							} else {
								die("Error, wrong signature!");
							}
						}
					}

					if ($_REQUEST['freekassa']=='success')
					{
						$this -> msg['message'] = "Thank you for shopping with us.";
                        $this -> msg['class'] = 'woocommerce_message woocommerce_message_info';
                        $order -> add_order_note('Free-Kassa payment status is success<br/>Unnique Id from Free-Kassa: '.$_REQUEST['intid']);
                        $order -> add_order_note($this->msg['message']);
                        $order -> update_status('on-hold');
                        $woocommerce -> cart -> empty_cart();
						wp_redirect("http://www.blvcks.com/success");
						exit;
					}

					if ($_REQUEST['freekassa']=='cancel')
					{
						$this -> msg['class'] = 'woocommerce_error';
                        $this -> msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
                        $order -> add_order_note('Transaction Declined');
						wp_redirect("http://www.blvcks.com/fail");
						exit;
					}


				}
				catch (Exception $e)
				{
		            $msg = "Error";
		        }

		        
		 
		    }
		 
		    function showMessage($content){
				return '<div class="box '.$this -> msg['class'].'-box">'.$this -> msg['message'].'</div>'.$content;
		    }
		    
		     // get all pages
		    function get_pages($title = false, $indent = true) {
		        $wp_pages = get_pages('sort_column=menu_order');
		        $page_list = array();
		        if ($title) $page_list[] = $title;
		        foreach ($wp_pages as $page) {
		            $prefix = '';
		            // show indented child pages?
		            if ($indent) {
		                $has_parent = $page->post_parent;
		                while($has_parent) {
		                    $prefix .=  ' - ';
		                    $next_page = get_page($has_parent);
		                    $has_parent = $next_page->post_parent;
		                }
		            }
		            // add to page list array array
		            $page_list[$page->ID] = $prefix . $page->post_title;
		        }
		        return $page_list;
		    }

		}
		//end WC_Gateway_FreeKassa class 


		/**
		* Add the Gateway to WooCommerce
		**/
		function woocommerce_add_freekassa_gateway($methods) {
		    $methods[] = 'WC_Gateway_FreeKassa';
		    return $methods;
		}
	 
	    add_filter('woocommerce_payment_gateways', 'woocommerce_add_freekassa_gateway' );
	}


?>
