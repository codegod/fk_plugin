# Setting on Free-kassa.ru

1. On site Free-kassa.ru in shop settings mean:

2. URL notification:  http://sitename/?wc-api=freekassa_callback_response?freekassa=result

3. URL success:  http://sitename/?wc-api=freekassa_callback_response?freekassa=success

4. URL fail:  http://sitename/?wc-api=freekassa_callback_response?freekassa=cancel

5. Data sending method "POST".

# Plugin install.

 1. Enter into admin panel.

 2. Enter into menu "Plugins" -> "Add new" -> "Upload plugin", upload the plugin, is the ".zip" file in folder.

 3. Install.

# Plugin settings

 1. Enter in menu "Woocommerce" -> "Settings"

 2. Click on section "Checkout"

 3. Find Freekassa and click on it.
 
 4. Enter data of your shop.